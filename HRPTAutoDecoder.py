import subprocess
from shutil import  disk_usage
from time import sleep
from queue import Queue
from datetime import datetime

from Config import HRPTDecoderConfig, HRPTDecoderConfigLoader
from pathlib import Path

from threading import Thread
from pymsgbox import alert

__version__ = "1.0.3"

#TODO Add processing at a certain time



# linux cant use unlink for dirs
def rmtree(f: Path):
    if f.is_file():
        f.unlink()
    else:
        for child in f.iterdir():
            rmtree(child)
        f.rmdir()
        
def dirSize(root: Path):
    return sum(f.stat().st_size for f in root.glob('**/*') if f.is_file())

class HRPTAutoDecoder:
    """
    A class that monitors and automatically decodes satellite images
    """
    def __init__(self, config: HRPTDecoderConfig) -> None:
        self.config = config
        self.dataPath = config.dataPath
        self.satDumpPath = config.satDumpPath
        self.checkDirInterval = config.checkDirInterval
        
        self.logPath = config.logPath
        self.logPath.mkdir(parents=True, exist_ok=True)
        self.logger = config.logger
        
        self.satellites = config.satellites
        self.satDump = config.satDump
        self.spaceLimit = config.spaceLimit
        
        self.basebandFormat = config.bf
        
        self.decodeQueue = Queue()
        self.detectedFiles = []
        
        self.keepBad = config.keepBad
        self.keepGood = config.keepGood
        
        self.__version__ = __version__
        
        self.pause = False
        self.work = False
        
        self.checkDirThread = Thread(target=self.checkDir, daemon=True)
        self.decodeThread = Thread(target=self.decode, daemon=True)
            

    def checkDirTick(self) -> None:                        
        for path in self.dataPath:
            # if If the file is .iq, it has not been decoded before and is not being written to the file now
            files = [i for i in path.iterdir() if i.name.endswith(".iq") and ( path / i not in self.detectedFiles)]
            
            for file in files:
                t =  file.stat()

                # if the file was modified earlier than 10 minutes ago
                                    
                if (datetime.now() - datetime.fromtimestamp(t.st_mtime)).total_seconds() < 600:
                    sleep(2)
                    # Check if there is no writing to the file now
                    # In linux, start.st_size changes as new data is written
                    # windows Explorer immediately allocates full space for the file being written,
                    # so only stat.st_mtime changes - the time of the last modification (Once per second)
                    if t != (path / file).stat():
                        continue 

                # Checking the write availability of the file
                open(file, 'a').close()
                
                if file.name.split(".")[0][16:] in self.satellites.keys():
                    self.logger.info(f"Found new file: '{file}'")
                    self.detectedFiles.append(path / file)
                    self.decodeQueue.put(path / file)

    def checkDir(self) -> None:
        while self.work:            
            try:    
                if self.pause:
                    sleep(10)
                    self.logger.warning("Decoder paused. Wait 10 seconds")
                    continue
                
                self.checkDirTick()
            
            except IOError:
                sleep(self.checkDirInterval + .1)
                continue
                
            except Exception as e:
                self.logger.error(f"Error at the time check dir: {str(e)}")
                
            sleep(self.checkDirInterval + .1)


    def decodeTick(self) -> None:
        
        for data in self.dataPath:
            totalSpace, freeSpace = disk_usage(data).total / 1024 / 1024 / 1024, disk_usage(data).free / 1024 / 1024 / 1024    
                        
            # if free space < spaceLimit in GB
            if freeSpace < self.spaceLimit:
                self.pause = True
                
                self.logger.warning(f"No free space: {freeSpace:.2f} Gb < {self.spaceLimit:.2f} Gb")
                alert(f"No free space: {freeSpace:.2f} Gb < {self.spaceLimit:.2f} Gb", "No free space", timeout=self.config.noFreeSpaceTimeout)
                sleep(10)
                break
            
        else:
            self.pause = False                                            
        
        if self.decodeQueue.empty():
            sleep(.1)
            return
        
        item = self.decodeQueue.get(block=False)
        path, file = item.parent, item.name
        
        if not item.exists():
            sleep(.1)
            return
                        
        satellite = file.split(".")[0][16:]

        output = path / "decoded" / file.split('.')[0]

        if not output.exists():
            output.mkdir(parents=True, exist_ok=True)
            
        self.logger.info(f"Decode '{file}' is started. Free space left: {freeSpace:.2f} / {totalSpace:.2f} GB")
        
        command = f'"{ self.satDumpPath / self.satDump }" {self.satellites[satellite]["pipeline"]} baseband "{ path / file }" "{ output }" --samplerate {self.satellites[satellite]["samplerate"]} --baseband_format {self.basebandFormat}'
                
        self.logger.info("SatDump command:\n" + command)
        
        sleep(self.checkDirInterval + 0.1)
                        
        with subprocess.Popen(command, shell=True, cwd=self.satDumpPath) as process:
            code = process.wait()

            outParts = [i for i in output.iterdir()]

            # If there is no Tool directory after processing
            if not len([i for i in outParts if (i.name == "MSU-MR") or (i.name == "AVHRR")]):
                rmtree(output)
                
                if not self.keepBad:
                    self.logger.info(f"Remove {file}")
                    (path / file).unlink()

                self.logger.warning(f"'{file}' No data - decoding failed")
            
            # If bad data is received after processing
            elif ((output / "MSU-MR").exists() and dirSize(output / "MSU-MR") < 15*1024*1024) or \
                ((output / "AVHRR").exists() and dirSize(output / "AVHRR") < 15*1024*1024):
                rmtree(output)
                    
                if not self.keepBad:
                    self.logger.info(f"Remove {file}")
                    (path / file).unlink()

                self.logger.warning(f"'{file}' Bad data - decoding failed")
            
            else:
                if not self.keepGood:
                    (path / file).unlink()
                    self.logger.info(f"Remove {file}") 
                            
                self.logger.info(f"'{file}' Decoding successfull")      
                    
        
    def decode(self) -> None:
        while self.work:
            try:
                if self.pause:
                    sleep(.1)
                    continue 
                
                self.decodeTick()
                            
            except Exception as e:
                self.logger.error(f"Error: {str(e)} in self.decode")
                
            sleep(.01)

    
    def run(self) -> None:
        self.logger.info(f"Decoder {__version__} started")

        self.work = True

        self.checkDirThread.start()
        self.decodeThread.start()
    
    #  It can work indefinitely if you do not stop the application using self.stop() or ctr+c in the terminal
    def join(self) -> None:
        while self.checkDirThread.is_alive() and self.decodeThread.is_alive():
            try:
                sleep(1)  
            except KeyboardInterrupt:
                self.logger.info(f"Manual exit with KeyboardInterrupt")
                return

    def stop(self) -> None:
        self.work = False
        self.logger.info("Decoder was turned off\n\n")

        
        
if __name__ == "__main__":
    config = HRPTDecoderConfigLoader("config.json")
    decoder = HRPTAutoDecoder(config=config)
    
    decoder.run()
    decoder.join()
