from pathlib import Path
from json import dump, load
from dataclasses import dataclass
from pprint import pformat
from sys import platform

from Logger import Logger, loggingLevels
from verboselogs import VerboseLogger, VERBOSE


config = {
    "satDumpPath": "C:\\Lorett\\SatDump" if platform == "win32" else "~/lorett/satdump",
    "dataPath": ["C:\\Lorett\\data"] if platform == "win32" else ["~/lorett/data"],
    "logPath": "",
    
    "satellites": {
        "FENGYUN 3C": {"pipeline": "fengyun3_c_ahrpt", "samplerate": "6000000"},

        "METOP-B": {"pipeline": "metop_ahrpt", "samplerate": "6000000"},
        "METOP-C": {"pipeline": "metop_ahrpt", "samplerate": "6000000"},

        "NOAA 18": {"pipeline": "noaa_hrpt", "samplerate": "6000000"},
        "NOAA 19": {"pipeline": "noaa_hrpt", "samplerate": "6000000"},

        "METEOR-M2 2": {"pipeline": "meteor_hrpt", "samplerate": "6000000"},
        "METEOR-M2 3": {"pipeline": "meteor_hrpt", "samplerate": "6000000"},
        "METEOR-M 2": {"pipeline": "meteor_hrpt", "samplerate": "6000000"},    
    }, 
    
    "bf": "f32",
    "loggingLevel": "verbose",
    "checkDirInterval": 1,
    
    "spaceLimit": 20,
    "noFreeSpaceTimeout": 0,

    "keepGood": False,
    "keepBad": False
}


@dataclass
class HRPTDecoderConfig:
    satDumpPath: Path
    dataPath: Path
    
    satellites: dict
    
    bf: str
    
    loggingLevel: int
    checkDirInterval: int
    
    keepGood: bool
    keepBad: bool
    
    spaceLimit: int
    noFreeSpaceTimeout: int
    
    logger: VerboseLogger
    
    
class HRPTDecoderConfigLoader(HRPTDecoderConfig):
    def __init__(self, path: str) -> None:
        self.__path = Path(path)
        
        if not self.__path.exists():
            self.loggingLevel = loggingLevels[config["loggingLevel"]]
            self.logPath = Path(config["logPath"])
            
            self.logPath.mkdir(parents=True, exist_ok=True)
            
            self.logger = Logger("decoder", self.logPath, self.loggingLevel)
            self.logger.error(f"File {self.__path} is not exists")
            self.logger.info("Try use default config")  
            
            self.satDumpPath = Path(config["satDumpPath"])
            
            self.dataPath = [Path(i) for i in config["dataPath"]]
            for i in self.dataPath:
                i.mkdir(parents=True, exist_ok=True)
            
            self.satellites = config["satellites"]
            
            self.bf = config["bf"]
            
            self.checkDirInterval = config["checkDirInterval"]
            
            self.spaceLimit = config["spaceLimit"]
            self.noFreeSpaceTimeout = config["noFreeSpaceTimeout"] if config['noFreeSpaceTimeout'] > 0 else None
            
            self.keepBad = config["keepBad"]
            self.keepGood = config["keepGood"]
            
            self.logger.verbose("\n" + pformat(config, indent=4, width=120, sort_dicts=False))
            
            self.save()
            
        else:
            with open(self.__path, "r") as f:
                fromJson = load(f)
                
                missed = tuple(i for i in config.keys() if i not in fromJson)
                                              
                self.loggingLevel = loggingLevels[fromJson.get("loggingLevel", config["loggingLevel"])]
                self.logPath = Path(fromJson.get("logPath", config["logPath"]))
                
                self.logger = Logger("decoder", self.logPath, self.loggingLevel)
                
                for i in missed:
                    self.logger.warning(f"{i} key missed in config. Try use default: {config[i]}")
                    fromJson[i] = config[i]
                                
                self.satDumpPath = Path(fromJson["satDumpPath"])
                self.dataPath = [Path(i) for i in fromJson["dataPath"]]
                
                for i in self.dataPath:
                    i.mkdir(parents=True, exist_ok=True)
                
                self.satellites = fromJson["satellites"]
                
                self.bf = fromJson["bf"]
                
                self.spaceLimit = fromJson["spaceLimit"]
                self.noFreeSpaceTimeout = fromJson["noFreeSpaceTimeout"] if fromJson["noFreeSpaceTimeout"] > 0 else None
                
                self.checkDirInterval = fromJson["checkDirInterval"]
                self.keepBad = fromJson["keepBad"]
                self.keepGood = fromJson["keepGood"]
                
                if len(missed):
                    self.save()  
                
                self.logger.verbose("\n" + pformat(fromJson, indent=4, width=120, sort_dicts=False))                          

        for i in self.dataPath:
            if not i.exists():
                self.logger.critical(f"{i} is not exists")
                raise FileNotFoundError(f"DataPath directory not found: {i}")

        if not self.satDumpPath.exists():
            self.logger.critical(f"{self.satDumpPath} is not exists")
            raise FileNotFoundError(f"SatDumpPath directory not found: {i}")

        # Platform check
        if platform == "win32":
            self.satDump = "satdump.exe"
        else:
            self.satDump = "satdump"

        if not (self.satDumpPath / self.satDump).exists():
            self.logger.critical("SatDump executable file is not found. Exit")
            raise FileNotFoundError(f"SatDump executable file is not found: {self.satDumpPath / self.satDump}")
        
    def save(self) -> None:
        with open(self.__path, "w") as f:
            self.logger.info(f"Save config to {self.__path}")
            
            dump({
                "satDumpPath": str(self.satDumpPath),
                "dataPath": [ str(i) for i in self.dataPath],
                "logPath": str(self.logPath),
                
                "satellites": self.satellites,
                
                "bf": self.bf,
                "loggingLevel": loggingLevels[self.loggingLevel],
                "spaceLimit": self.spaceLimit,
                "noFreeSpaceTimeout": 0 if self.noFreeSpaceTimeout is None else self.noFreeSpaceTimeout,
                "checkDirInterval": self.checkDirInterval,

                "keepGood": self.keepGood,
                "keepBad": self.keepBad
            }, f, indent=4, sort_keys=False)